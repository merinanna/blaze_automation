package com.safaricom.config;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class TestConfig {
	private static TestConfig instance=null;
	
	private AndroidDriver<MobileElement> driver;
	private WebDriverWait wait;
	
	private TestConfig() 
    {
		
		//ANDROID
		
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("deviceName", "Redmi");
		capabilities.setCapability("udid", "3300e1dd1655638d");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("platformVersion", "6.0.1"); // com.selfcare.safaricom/com.mventus.selfcare.safaricom.activity.SplashScreenActivity
		capabilities.setCapability("appPackage", "com.selfcare.safaricom");
		capabilities.setCapability("appActivity", "com.mventus.selfcare.safaricom.activity.SplashScreenActivity");
		// capabilities.setCapability("skipUnlock", "true");
		capabilities.setCapability("noReset", "true");
		capabilities.setCapability("unicodeKeyboard", "false");
		capabilities.setCapability("resetKeyboard", "true");
		//capabilities.setCapability("dontStopAppOnReset", "true");
		
		
//		//IOS
//		
//		DesiredCapabilities capabilities = new DesiredCapabilities();
//		capabilities.setCapability("deviceName", "iPhone 6S Plus");
//		capabilities.setCapability("udid", "84959323da6875a1202f4246576c057f6433eedd");
//		capabilities.setCapability("platformName", "ios");
//		capabilities.setCapability("platformVersion", "9.0.1"); // com.selfcare.safaricom/com.mventus.selfcare.safaricom.activity.SplashScreenActivity
//		capabilities.setCapability("appPackage", "com.selfcare.safaricom");
//		capabilities.setCapability("appActivity", "com.mventus.selfcare.safaricom.activity.SplashScreenActivity");
//		// capabilities.setCapability("skipUnlock", "true");
//		capabilities.setCapability("noReset", "true");
//		capabilities.setCapability("unicodeKeyboard", "false");
//		capabilities.setCapability("resetKeyboard", "true");
//		capabilities.setCapability("dontStopAppOnReset", "true");
		
		try {
			driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
			wait = new WebDriverWait(driver, 120);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

	public static TestConfig getInstance()  {
		if (instance == null) 
        { 
			instance = new TestConfig(); 
        } 
        return instance; 
	}

	public WebDriverWait getWait() {
		return wait;
	}

	public AppiumDriver<MobileElement> getDriver() {
		return driver;
	}
}
