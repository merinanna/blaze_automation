package com.safaricom.blazehome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ServicesHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public ServicesHomePage() {
	  
  }
  public ServicesHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
 
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement services_title;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Bonga Services']")
  public AndroidElement bongaservicesClick;
    

  @AndroidFindBy(xpath="//android.widget.TextView[@text='FLEX']")
  public AndroidElement flexClick ;

  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Data & SMS Plans']")
  public AndroidElement dataandsmsplansClick;
    

  @AndroidFindBy(xpath="//android.widget.TextView[@text='My SMS Services']")
  public AndroidElement mysmsservicesClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Sambaza Services']")
  public AndroidElement sambazaservicesClick;
    

  @AndroidFindBy(xpath="//android.widget.TextView[@text='Skiza Services']")
  public AndroidElement skizaservicesClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Roaming Services']")
  public AndroidElement roamingservicesClick;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Okoa Services']")
  public AndroidElement okoaservicesClick ;
  
   
}
