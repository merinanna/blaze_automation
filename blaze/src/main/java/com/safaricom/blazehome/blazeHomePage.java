package com.safaricom.blazehome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class blazeHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public blazeHomePage() {
	  
  }
  public blazeHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(className="android.widget.ImageButton")
  public AndroidElement menutab_click;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BE YOUR OWN BOSS']")
  public AndroidElement beyourboss;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE BUNDLES']")
  public AndroidElement blazebundles;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE DATA USAGE']")
  public AndroidElement blazedataUsage;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE BONGA']")
  public AndroidElement blazebonga;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE M-PESA']")
  public AndroidElement blazempesa;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BLAZE DEVICES']")
  public AndroidElement blazedevices;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='TALK TO US']")
  public AndroidElement talktoUs;
  
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/containerIcons")
  public AndroidElement notification_icon;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/fab_info")
  public AndroidElement info;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/tv_blaze_username")
  public AndroidElement tv_blaze_username;
  
      
}
