package com.safaricom.blazehome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class blazebundlesHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public blazebundlesHomePage() {
	  
  }
  public blazebundlesHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement blazebundles_title;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='DAILY']")
  public AndroidElement daily;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='WEEKLY']")
  public AndroidElement weekly;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='MONTHLY']")
  public AndroidElement monthly;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='POWER HOUR BUNDLES']")
  public AndroidElement powerhourbundles;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='CHECK BALANCE']")
  public AndroidElement checkbalance;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='MY PLAN HISTORY']")
  public AndroidElement myplanhistory;
  
}
