package com.safaricom.blazehome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class PlatinumPlansHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public PlatinumPlansHomePage() {
	  
  }
  public PlatinumPlansHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  @AndroidFindBy(xpath="//android.widget.TextView[@text='10K']")
  private AndroidElement plan_Click;
    

  @AndroidFindBy(id="com.selfcare.safaricom:id/view_all_plans")
  private AndroidElement view_all_plans_click ;

   
}
