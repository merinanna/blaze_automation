package com.safaricom.blazehome;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MpesaHomePage {
	public AndroidDriver<MobileElement> driver;
  
  public MpesaHomePage() {
	  
  }
  public MpesaHomePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/txt_title")
  public AndroidElement mpesa_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/show_balance")
  public AndroidElement mpesa_show_balance;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA Statement']")
  public AndroidElement mpesa_statement;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonScanQR")
  public AndroidElement mpesa_buttonScanQR;
   
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Send Money']")
  public AndroidElement sendMoneyClick;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Withdraw Cash']")
  public AndroidElement withdraCashClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Buy Airtime']")
  public AndroidElement buyairtimeClick;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Lipa na M-PESA']")
  public AndroidElement lipanampesaClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Bill Manager']")
  public AndroidElement billmanagerClick;
    
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Loans & Savings']")
  public AndroidElement loansandsavingsClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='My Account']")
  public AndroidElement myaccountClick;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Fuliza M-PESA']")
  public AndroidElement fulizampesaClick ;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='M-PESA Global']")
  public AndroidElement mpesaglobalClick;
    

  
  
}
