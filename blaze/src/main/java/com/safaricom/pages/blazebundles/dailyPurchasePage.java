package com.safaricom.pages.blazebundles;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class dailyPurchasePage {
	public AndroidDriver<MobileElement> driver;
  
  public dailyPurchasePage() {
	  
  }
  public dailyPurchasePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='BUY ONCE']")
  public AndroidElement buyOnce;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='AUTO RENEW']")
  public AndroidElement autoRenew;
      
}
