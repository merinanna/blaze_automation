package com.safaricom.pages.blazebundles;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class weeklyPage_Activate {
	public AndroidDriver<MobileElement> driver;
  
  public weeklyPage_Activate() {
	  
  }
  public weeklyPage_Activate(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='OK']")
  public AndroidElement ok_btn;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='Cancel']")
  public AndroidElement cancel_btn;
  
}
