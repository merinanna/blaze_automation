package com.safaricom.pages.blazebundles;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class daily_AutoRenew_Accept {
	public AndroidDriver<MobileElement> driver;
  
  public daily_AutoRenew_Accept() {
	  
  }
  public daily_AutoRenew_Accept(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/succss_message")
  public AndroidElement succss_message;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='OK']")
  public AndroidElement autoRenew_ok;
  
}
