package com.safaricom.pages.blazebundles;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class daily_BuyOnce {
	public AndroidDriver<MobileElement> driver;
  
  public daily_BuyOnce() {
	  
  }
  public daily_BuyOnce(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/title")
  public AndroidElement buy_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/message")
  public AndroidElement message;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='ACCEPT']")
  public AndroidElement buyonce_accept;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='DECLINE']")
  public AndroidElement buyonce_decline;
      
}
