package com.safaricom.pages.blazebundles;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class dailyPage {
	public AndroidDriver<MobileElement> driver;
  
  public dailyPage() {
	  
  }
  public dailyPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement daily_title;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/offerTitle")
  public AndroidElement offerTitle;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/offerDescription")
  public AndroidElement offerDescription;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonAction")
  public AndroidElement purchase_btn;
        
}
