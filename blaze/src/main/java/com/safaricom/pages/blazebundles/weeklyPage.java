package com.safaricom.pages.blazebundles;

import java.util.List;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class weeklyPage {
	public AndroidDriver<MobileElement> driver;
  
  public weeklyPage() {
	  
  }
  public weeklyPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @FindBy(how=How.ID,using="com.selfcare.safaricom:id/")
  public List<MobileElement> L1;        
}
