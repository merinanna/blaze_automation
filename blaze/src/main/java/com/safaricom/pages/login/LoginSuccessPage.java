package com.safaricom.pages.login;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginSuccessPage {
	
	public AndroidDriver<MobileElement> driver;
	public LoginSuccessPage() {
		
	}
	
	public LoginSuccessPage(AndroidDriver<MobileElement> driver) {
		this.driver = driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/et_enter_otp")
	public AndroidElement otp;
	@AndroidFindBy(id="com.selfcare.safaricom:id/term_condition")
	public AndroidElement termConditionClick;
	
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_terms")
	public AndroidElement termCondition_view;
	@AndroidFindBy(id="com.selfcare.safaricom:id/tv_otp_login")
	public AndroidElement OtpLoginButton;
	
}
