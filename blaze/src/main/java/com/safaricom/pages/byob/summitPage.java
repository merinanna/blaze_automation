package com.safaricom.pages.byob;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class summitPage {
	public AndroidDriver<MobileElement> driver;
  
  public summitPage() {
	  
  }
  public summitPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonMenu")
  public AndroidElement menutab_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonSearch")
  public AndroidElement searchbtn;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='SUMMITS']")
  public AndroidElement summits;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/iv_summit_banner")
  public AndroidElement summit_banner;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_speakers")
  public AndroidElement btn_speakers;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/btn_summit_topic")
  public AndroidElement btn_summit_topic;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement byob_title;
        
}
