package com.safaricom.pages.byob;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class summit_speakersPage {
	public AndroidDriver<MobileElement> driver;
  
  public summit_speakersPage() {
	  
  }
  public summit_speakersPage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
 
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement speaker_title;
        
}
