package com.safaricom.pages.byob;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class mentorProfilePage {
	public AndroidDriver<MobileElement> driver;
  
  public mentorProfilePage() {
	  
  }
  public mentorProfilePage(AndroidDriver<MobileElement> driver)
  {
	  this.driver=driver;
	  PageFactory.initElements(new AppiumFieldDecorator(driver),this);
  }
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonMenu")
  public AndroidElement menutab_click;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/buttonSearch")
  public AndroidElement searchbtn;
  
  @AndroidFindBy(xpath="//android.widget.TextView[@text='MENTOR PROFILES']")
  public AndroidElement mentorprofiles;
   
  @AndroidFindBy(id="com.selfcare.safaricom:id/iv_mentor")
  public AndroidElement mentor;
  
  @AndroidFindBy(id="com.selfcare.safaricom:id/toolbarTitle")
  public AndroidElement byob_title;
        
}
