package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazehome.blazeHomePage;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BlazeHomeTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public LocationPermissionPage locationPermissionObject = new LocationPermissionPage(driver);
	public ManagePhonePermissionPage managePhonePermissionObject = new ManagePhonePermissionPage(driver);
	public LoginGeneratePinPage loginGeneratePinObject = new LoginGeneratePinPage(driver);
	public LoginSuccessPage loginSuccessPageObject = new LoginSuccessPage(driver);
	public blazeHomePage blazeHomePageObject=new blazeHomePage(driver);
	public LoginFailurePage loginfailureObject = new LoginFailurePage(driver);

	
	@Test
	public void BLAZEHOME_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
 		Assert.assertEquals(true, blazeHomePageObject.menutab_click.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.beyourboss.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.blazebundles.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.blazedataUsage.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.blazebonga.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.blazempesa.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.talktoUs.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.notification_icon.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.info.isDisplayed());
		Assert.assertEquals(true, blazeHomePageObject.tv_blaze_username.isDisplayed());
	}

}
