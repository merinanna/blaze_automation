package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazehome.BYOBHomePage;
import com.safaricom.blazehome.blazeHomePage;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.byob.mentorProfilePage;
import com.safaricom.pages.byob.summitPage;
import com.safaricom.pages.byob.summit_speakersPage;
import com.safaricom.pages.byob.summit_summittopicsPage;
import com.safaricom.pages.byob.tvshowPage;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BYOBTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public LocationPermissionPage locationPermissionObject = new LocationPermissionPage(driver);
	public ManagePhonePermissionPage managePhonePermissionObject = new ManagePhonePermissionPage(driver);
	public LoginGeneratePinPage loginGeneratePinObject = new LoginGeneratePinPage(driver);
	public LoginSuccessPage loginSuccessPageObject = new LoginSuccessPage(driver);
	public BYOBHomePage byobHomePageObject=new BYOBHomePage(driver);
	public LoginFailurePage loginfailureObject = new LoginFailurePage(driver);
	public mentorProfilePage mentorProfileObject=new mentorProfilePage(driver);
	public summitPage summitPageObject=new summitPage(driver);
	public summit_speakersPage summit_speakerpageObject=new summit_speakersPage(driver);
	public summit_summittopicsPage summit_summittopicsPageObject=new summit_summittopicsPage(driver);
	public blazeHomePage blazeHomePageObject=new blazeHomePage(driver);
	public tvshowPage tvshowPageObject=new tvshowPage(driver);
	
	@Test
	public void BEYOURBOSS_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
		
		String expectedtitle="BYOB";
		String byobtitle=byobHomePageObject.byob_title.getText();
		Assert.assertEquals(byobtitle, expectedtitle);
		Assert.assertEquals(true, byobHomePageObject.mentorprofiles.isDisplayed());
		Assert.assertEquals(true, byobHomePageObject.summits.isDisplayed());
		Assert.assertEquals(true, byobHomePageObject.tvshow.isDisplayed());
	}

	
	@Test
	public void BEYOURBOSS_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.beyourboss)).click();
		String expectedtitle="BYOB";
		String byobtitle=byobHomePageObject.byob_title.getText();
		Assert.assertEquals(byobtitle, expectedtitle);
		wait.until(ExpectedConditions.elementToBeClickable(byobHomePageObject.mentorprofiles)).click();
		Assert.assertEquals(true, mentorProfileObject.mentorprofiles.isSelected());
		Assert.assertEquals(true, mentorProfileObject.mentor.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(mentorProfileObject.mentor)).click();
	}
	
	@Test
	public void BEYOURBOSS_TC_003() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.beyourboss)).click();
		wait.until(ExpectedConditions.elementToBeClickable(byobHomePageObject.summits)).click();
		wait.until(ExpectedConditions.elementToBeClickable(summitPageObject.btn_speakers)).click();
		String expectedtitle="SPEAKERS";
		String speaker_title=summit_speakerpageObject.speaker_title.getText();
		Assert.assertEquals(speaker_title, expectedtitle);
		driver.navigate().back();
		wait.until(ExpectedConditions.elementToBeClickable(summitPageObject.btn_summit_topic)).click();
		String expectedtitle1="SUMMIT TOPICS";
		String summittopics_title=summit_summittopicsPageObject.summittopics_title.getText();
		Assert.assertEquals(summittopics_title, expectedtitle1);
	}
	
	@Test
	public void BEYOURBOSS_TC_004() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.beyourboss)).click();
		wait.until(ExpectedConditions.elementToBeClickable(byobHomePageObject.tvshow)).click();
		wait.until(ExpectedConditions.elementToBeClickable(tvshowPageObject.btn_more_details)).click();
		String expectedtitle="TV SHOW";
		String tvshow_title=tvshowPageObject.tvshow_title.getText();
		Assert.assertEquals(tvshow_title, expectedtitle);
	}
}
