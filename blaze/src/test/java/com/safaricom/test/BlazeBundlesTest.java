package com.safaricom.test;

import java.io.IOException;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.safaricom.blazehome.blazeHomePage;
import com.safaricom.blazehome.blazebundlesHomePage;
import com.safaricom.config.TestConfig;
import com.safaricom.pages.blazebundles.dailyPage;
import com.safaricom.pages.blazebundles.dailyPurchasePage;
import com.safaricom.pages.blazebundles.daily_AutoRenew;
import com.safaricom.pages.blazebundles.daily_AutoRenew_Accept;
import com.safaricom.pages.blazebundles.daily_BuyOnce;
import com.safaricom.pages.blazebundles.daily_BuyOnce_Accept;
import com.safaricom.pages.blazebundles.weeklyPage;
import com.safaricom.pages.blazebundles.weeklyPage_Activate;
import com.safaricom.pages.login.LocationPermissionPage;
import com.safaricom.pages.login.LoginFailurePage;
import com.safaricom.pages.login.LoginGeneratePinPage;
import com.safaricom.pages.login.LoginSuccessPage;
import com.safaricom.pages.login.ManagePhonePermissionPage;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

@Test
public class BlazeBundlesTest {
	ExtentReports extentReport;
	ExtentTest extentTest;
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AndroidDriver<MobileElement> driver = (AndroidDriver<MobileElement>) TestConfig.getInstance().getDriver();
	public LocationPermissionPage locationPermissionObject = new LocationPermissionPage(driver);
	public ManagePhonePermissionPage managePhonePermissionObject = new ManagePhonePermissionPage(driver);
	public LoginGeneratePinPage loginGeneratePinObject = new LoginGeneratePinPage(driver);
	public LoginSuccessPage loginSuccessPageObject = new LoginSuccessPage(driver);
	public blazeHomePage blazeHomePageObject=new blazeHomePage(driver);
	public blazebundlesHomePage blazebundlesHomePageObject=new blazebundlesHomePage(driver);
	public LoginFailurePage loginfailureObject = new LoginFailurePage(driver);
	public dailyPage dailyPageObject=new dailyPage(driver);
	public dailyPurchasePage dailyPurchasePageObject=new dailyPurchasePage(driver);
	public daily_BuyOnce daily_BuyOnceObject=new daily_BuyOnce(driver);
	public daily_BuyOnce_Accept daily_BuyOnce_AcceptObject=new daily_BuyOnce_Accept(driver);
	public daily_AutoRenew daily_AutoRenewObject=new daily_AutoRenew(driver);
	public daily_AutoRenew_Accept daily_AutoRenew_AcceptObject=new daily_AutoRenew_Accept(driver);
	public weeklyPage weeklyPageObject=new weeklyPage(driver);
	public weeklyPage_Activate weeklyPage_ActivateObject=new weeklyPage_Activate(driver);
	
	@Test
	public void BLAZEBUNDLES_TC_001() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.blazebundles)).click();
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}

	@Test
	public void BLAZEBUNDLES_TC_002() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundlesHomePageObject.daily)).click();
		String expectedtitle="DAILY";
		String daily_title=dailyPageObject.daily_title.getText();
		Assert.assertEquals(daily_title, expectedtitle);
		Assert.assertEquals(true, dailyPageObject.offerTitle.isDisplayed());
		Assert.assertEquals(true, dailyPageObject.offerDescription.isDisplayed());
		Assert.assertEquals(true, dailyPageObject.purchase_btn.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_003() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundlesHomePageObject.daily)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dailyPageObject.purchase_btn)).click();
		Assert.assertEquals(true, dailyPurchasePageObject.buyOnce.isDisplayed());
		Assert.assertEquals(true, dailyPurchasePageObject.autoRenew.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(dailyPurchasePageObject.buyOnce)).click();
		Assert.assertEquals(true, daily_BuyOnceObject.buy_title.isDisplayed());
		String expectedtitle="BUY";
		String buy_title=daily_BuyOnceObject.buy_title.getText();
		Assert.assertEquals(buy_title, expectedtitle);
		Assert.assertEquals(true, daily_BuyOnceObject.message.isDisplayed());
		Assert.assertEquals(true, daily_BuyOnceObject.buyonce_accept.isDisplayed());
		Assert.assertEquals(true, daily_BuyOnceObject.buyonce_decline.isDisplayed());
		wait.until(ExpectedConditions.elementToBeClickable(daily_BuyOnceObject.buyonce_accept)).click();
		wait.until(ExpectedConditions.elementToBeClickable(daily_BuyOnce_AcceptObject.buyOnce_ok)).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(dailyPageObject.purchase_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dailyPurchasePageObject.buyOnce)).click();
		wait.until(ExpectedConditions.elementToBeClickable(daily_BuyOnceObject.buyonce_decline)).click();
		
	}
	
	@Test
	public void BLAZEBUNDLES_TC_004() throws InterruptedException, IOException {

		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundlesHomePageObject.daily)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dailyPageObject.purchase_btn)).click();
		wait.until(ExpectedConditions.elementToBeClickable(dailyPurchasePageObject.autoRenew)).click();
		wait.until(ExpectedConditions.elementToBeClickable(daily_AutoRenewObject.autorenew_accept)).click();
		wait.until(ExpectedConditions.elementToBeClickable(daily_AutoRenew_AcceptObject.autoRenew_ok)).click();
		wait.until(ExpectedConditions.elementToBeClickable(daily_AutoRenewObject.autorenew_decline)).click();
		
	}
	
	@Test
	public void BLAZEBUNDLES_TC_005() throws InterruptedException, IOException {
		
		Thread.sleep(1000);
		wait.until(ExpectedConditions.elementToBeClickable(blazeHomePageObject.blazebundles)).click();
		wait.until(ExpectedConditions.elementToBeClickable(blazebundlesHomePageObject.weekly)).click();
		String expectedtitle="BLAZE BUNDLES";
		String main_title=weeklyPageObject.L1.get(0).getText();
		Assert.assertEquals(main_title, expectedtitle);
		String expectedtitle1="WEEKLY";
		String sub_title=weeklyPageObject.L1.get(1).getText();
		Assert.assertEquals(sub_title, expectedtitle1);
		wait.until(ExpectedConditions.elementToBeClickable(weeklyPageObject.L1.get(3))).click();
		wait.until(ExpectedConditions.elementToBeClickable(weeklyPage_ActivateObject.ok_btn)).click();	
		
	}
	
	@Test
	public void BLAZEBUNDLES_TC_006() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_007() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_008() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_009() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_010() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_011() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_012() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_013() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_014() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_015() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_016() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}
	
	@Test
	public void BLAZEBUNDLES_TC_017() throws InterruptedException, IOException {

		Thread.sleep(1000);
		String expectedtitle="BLAZE BUNDLES";
		String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
		Assert.assertEquals(blazebundlestitle, expectedtitle);
		Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
		Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
	}


@Test
public void BLAZEBUNDLES_TC_018() throws InterruptedException, IOException {

	Thread.sleep(1000);
	String expectedtitle="BLAZE BUNDLES";
	String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
	Assert.assertEquals(blazebundlestitle, expectedtitle);
	Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
}

@Test
public void BLAZEBUNDLES_TC_019() throws InterruptedException, IOException {

	Thread.sleep(1000);
	String expectedtitle="BLAZE BUNDLES";
	String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
	Assert.assertEquals(blazebundlestitle, expectedtitle);
	Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
}

@Test
public void BLAZEBUNDLES_TC_020() throws InterruptedException, IOException {

	Thread.sleep(1000);
	String expectedtitle="BLAZE BUNDLES";
	String blazebundlestitle=blazebundlesHomePageObject.blazebundles_title.getText();
	Assert.assertEquals(blazebundlestitle, expectedtitle);
	Assert.assertEquals(true, blazebundlesHomePageObject.daily.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.weekly.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.monthly.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.powerhourbundles.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.checkbalance.isDisplayed());
	Assert.assertEquals(true, blazebundlesHomePageObject.myplanhistory.isDisplayed());
}
}
