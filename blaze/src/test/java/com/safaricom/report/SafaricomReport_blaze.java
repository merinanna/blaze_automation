package com.safaricom.report;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.safaricom.config.TestConfig;
import com.safaricom.test.BYOBTest;
import com.safaricom.test.BlazeBundlesTest;
import com.safaricom.test.BlazeHomeTest;
import com.safaricom.test.LoginTest;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SafaricomReport_blaze {
	ExtentReports extentReport;
	ExtentTest extentTest;
	LoginTest logintestestObject = new LoginTest();
	BlazeHomeTest blazehometestObject=new BlazeHomeTest();
	BYOBTest byobtestObject=new BYOBTest();
	BlazeBundlesTest blazebundlesObject=new BlazeBundlesTest();
	
	
	
	
	WebDriverWait wait = TestConfig.getInstance().getWait();
	AppiumDriver<MobileElement> driver = (AppiumDriver<MobileElement>) TestConfig.getInstance().getDriver();

	@BeforeSuite
	public void beforeSuite() {
		// In before suite we are creating HTML report template, adding basic
		// information to it and load the extent-config.xml file

		extentReport = new ExtentReports(System.getProperty("user.dir") + "/blaze_Report.html", true);
		extentReport.addSystemInfo("Host Name", "Safaricom").addSystemInfo("Environment", "Automation Testing")
				.addSystemInfo("User Name", "Merin Anna Mathew");
		extentReport.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));
	}

	@BeforeClass
	public void beforeClass() {
		// WebDriverWait wait = TestConfig.getInstance().getWait();
		// AppiumDriver<MobileElement> driver = TestConfig.getInstance().getDriver();
	}

	@BeforeMethod
	public void beforeMethod(Method method) {
		// In before method we are collecting the current running test case name
		String className = this.getClass().getSimpleName();
		extentTest = extentReport.startTest(className + "-" + method.getName());
	}

	
	@Test(priority=1)
	public void LOGIN_TC_001() throws InterruptedException, IOException {

		logintestestObject.LOGIN_TC_001();
		Assert.assertTrue(true);
	}

	@Test(priority=2)
	public void SFC_TC_001() throws InterruptedException, IOException {

		blazehometestObject.BLAZEHOME_TC_001();
		Assert.assertTrue(true);

	}

	@Test(priority=3)
	public void MPESA_TC_001() throws InterruptedException, IOException {

		byobtestObject.BEYOURBOSS_TC_001();
		Assert.assertTrue(true);
	}

	@Test(priority=4)
	public void SENDMONEY_TC_001() throws InterruptedException, IOException {

		blazebundlesObject.BLAZEBUNDLES_TC_001();
		Assert.assertTrue(true);
	}

	

	@AfterMethod
	public void getResult(ITestResult result, Method method) throws Exception {
		// In after method we are collecting the test execution status and based on that
		// the information writing to HTML report
		if (result.getStatus() == ITestResult.FAILURE) {
			extentTest.log(LogStatus.FAIL, "Test Case Failed is : " + result.getName());
			// String screenshotPath = SafaricomReport.capture(driver, result.getName());
			extentTest.log(LogStatus.FAIL, "Error Details :- "  +  result.getThrowable().getMessage() );// +extentTest.addScreenCapture(screenshotPath));
			driver.closeApp();
			driver.launchApp();
		}
		if (result.getStatus() == ITestResult.SKIP) {
			extentTest.log(LogStatus.SKIP, "Test Case Skipped is : " + result.getName());
		}
		if (result.getStatus() == ITestResult.SUCCESS) {
			extentTest.log(LogStatus.PASS, "Test Case Passed is : " + result.getName());
		}
	}

	@AfterSuite
	public void endReport() {
		// In after suite stopping the object of ExtentReports and ExtentTest
		extentReport.endTest(extentTest);
		extentReport.flush();
		driver.quit();
	}

	/**
	 * 
	 * To Capture the Screenshot and return the file path to extent report fail
	 * cases
	 *
	 * @param driver
	 * @param screenShotName
	 * @return
	 * @throws IOException
	 */
	public static String capture(AppiumDriver<MobileElement> driver, String screenShotName) throws IOException {
		String dest = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy h-m-s");
			Date date = new Date();
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			dest = System.getProperty("user.dir") + "/ErrorScreenshots/" + screenShotName + dateFormat.format(date)
					+ ".png";
			File destination = new File(dest);
			FileUtils.copyFile(source, destination);
		} catch (Exception e) {
			e.getMessage();
			System.out.println(e.getMessage());
		}
		return dest;
	}
}
